<?php 

	require_once 'mail-handler.php';

	if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['title']) && !empty($_POST['message'])) {
		$result = sendMail($_POST);
		if ($result == 'OK') {
			$response['status'] = 'ok';
			$response['message'] = 'E-mail enviado com sucesso! :)';
		} else {
			$response['status'] = 'error';
			$response['message'] = 'Infelizmente, houve falha no envio do e-mail :('
		}
	} else {
		$response['status'] = 'error';
		$response['message'] = 'Algum campo obrigatório não foi preenchido.';
	}

	$json = json_encode($response);
	die($json);

?>
