<?php

	require_once '/vendor/autoload.php';

	function sendMail($data){
		$mail_to = 'iagosilvany@hotmail.com';
		$mail_subject = 'Heitor! Alguém preencheu seu formulário :)';

		$mail_message = 'Opa, e aí, beleza?<br> Se liga, alguém preencheu o formulário do seu site...<br>';
		$mail_message .= 'O nome da pessoa é '.$data['nome'].'<br>';
		$mail_message .= 'O e-mail da pessoa é '.$data['email'].'<br>';
		$mail_message .= 'Essa pessoa quer falar sobre: '.$data['title'].'<br>';
		$mail_message .= 'O que ela escreveu: '.$data['message'].'<br><br><br>';
		$mail_message .= 'Não perca tempo! Não deixe o contatinho esperando ;)';


		// Create the Transport
		$transport = (new Swift_SmtpTransport('smtp.example.org', 25))
			->setUsername('your username')
			->setPassword('your password')
		;

		// Create the Mailer using your created Transport
		$mailer = new Swift_Mailer($transport);

		// Create a message
		$message = (new Swift_Message($mail_message))
			->setFrom(['iagosilvany@gmail.com' => 'Iago Silvany GMail'])
			->setTo(['iagosilvany@hotmail.com' => 'Iago Silvany Hotmail'])
			->setBody($mail_message)
		;

		// Send the message
		$result = $mailer->send($message);
		return $result;
	}

?>
